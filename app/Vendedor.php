<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    protected $table = 'trk_vendedores';

    protected $fillable = ['id', 'usuario', 'nombres',  'email', 'celular'];
}

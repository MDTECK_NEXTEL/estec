<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'trk_productos';

    protected $fillable = ['id', 'codigo', 'descripcion', 'unidad', 'cantidad', 'estado','pedidos_numero'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'trk_clientes';

    protected $fillable = ['id','codigo', 'usuario', 'nombres','clave', 'ruc', 'email', 'direccion_fiscal', 'direccion_entrega', 'vendedor_id'];
}

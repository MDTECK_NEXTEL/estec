<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoEspecificacionTecnica extends Model
{
    protected $table = 'trk_productos_EspecificacionTecnica';

    protected $fillable = ['id', 'producto_id','url'];
}

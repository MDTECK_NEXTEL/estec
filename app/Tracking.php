<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = 'trk_pedidos';

    protected $fillable = ['Agencia', 'NumPedido', 'OrdenCompra', 'NumGuiaRemision', 'FechaPedido', 'CodigoCliente', 'Estado'];

}


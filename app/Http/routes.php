<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('/login/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('/login/', 'Auth\AuthController@postLogin');
Route::get('/logout/', 'Auth\AuthController@getLogout');

// registracion
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
// registracion autenticate

// Historial
Route::get('/home','Tracking\TrackingController@index');
Route::get('/historial/','Tracking\TrackingController@index');
Route::get('/historial/{NumPedido}', 'Tracking\TrackingController@getHistorial');
// Historial

// cliente
Route::get('/cliente/','Usuarios\ClienteController@index');
Route::get('/cliente/{id}','Usuarios\ClienteController@getClientes');
// cliente

// vendedor
Route::get('/vendedor/','Usuarios\VendedorController@index');
// vendedor

Route::get('/contactenos/', function () {
    return view('sistema.contactenos.contactenos');
});

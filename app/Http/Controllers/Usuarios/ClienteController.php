<?php

namespace App\Http\Controllers\Usuarios;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cliente;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $clientes = Cliente::all();
            return view('sistema.mantenimiento.clientes')->with('clientes', $clientes);
        }
        else{
            return redirect('/login/');
        }
    }


    public function getClientes(){
        if (Auth::check()) {

            $clientes =  new Cliente();
            $retorno = $clientes.

            $cliente = DB::table('trk_clientes AS cl')
                ->select('cl.usuario',
                         'cl.nombres',
                         'cl.nombres',
                         'cl.ruc',
                         'cl.email',
                         'cl.clave',
                         'cl.direccion_fiscal',
                         'cl.vendedor_id')


            $pedido = DB::table('trk_ AS pe')
                ->leftJoin('trk_clientes AS cl', 'pe.CodigoCliente', '=', 'cl.codigo')
                ->leftJoin('trk_vendedores AS ve', 'cl.vendedor_id', '=', 've.id')
                ->select('cl.nombres',
                    've.nombres AS vendedores',
                    'pe.Agencia',
                    'pe.NumPedido',
                    'pe.OrdenCompra',
                    'pe.NumGuiaRemision',
                    'pe.FechaPedido',
                    'pe.CodigoCliente',
                    'pe.Estado')
                ->where('pe.NumPedido', '=', $NumPedido)
                ->get();

            $productos = DB::table('trk_productos AS pr')
                ->join('trk_pedidos AS pe', 'pe.NumPedido', '=', 'pr.pedidos_numero')
                ->join('trk_productos_EspecificacionTecnica AS pre', 'pre.producto_id', '=', 'pr.id')
                ->select('pre.url', 'pr.codigo', 'pr.descipcion', 'pr.unidad', 'pr.cantidad', 'pr.estado', 'pr.pedidos_numero', 'pr.id')
                ->where('pe.NumPedido', '=', $NumPedido)
                ->get();

            $retorno = new stdObject();
            $retorno->cuerpo = $pedido;
            $retorno->detalle = $productos;

            return json_encode($retorno);
        }
        else{
            return redirect('/login/');
        }
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

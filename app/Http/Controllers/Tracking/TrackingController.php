<?php

namespace App\Http\Controllers\Tracking;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tracking;
use App\Producto;
use App\ProductoEspecificacionTecnica;
use App\Util\stdObject;
use DB;
use Illuminate\Support\Facades\Auth;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $historial = Tracking::all();
            return view('sistema.tracking.historial')->with('historial', $historial);
        }
        else{
            return redirect('/login/');
        }
    }



    public function getHistorial($NumPedido)
    {
        if (Auth::check()) {
            $pedido = DB::table('trk_pedidos AS pe')
                ->leftJoin('trk_clientes AS cl', 'pe.CodigoCliente', '=', 'cl.codigo')
                ->leftJoin('trk_vendedores AS ve', 'cl.vendedor_id', '=', 've.id')
                ->select('cl.nombres',
                         've.nombres AS vendedores',
                         'pe.Agencia',
                         'pe.NumPedido',
                         'pe.OrdenCompra',
                         'pe.NumGuiaRemision',
                         'pe.FechaPedido',
                         'pe.CodigoCliente',
                         'pe.Estado')
                ->where('pe.NumPedido', '=', $NumPedido)
                ->get();

            $productos = DB::table('trk_productos AS pr')
                ->join('trk_pedidos AS pe', 'pe.NumPedido', '=', 'pr.pedidos_numero')
                ->join('trk_productos_EspecificacionTecnica AS pre', 'pre.producto_id', '=', 'pr.id')
                ->select('pre.url', 'pr.codigo', 'pr.descipcion', 'pr.unidad', 'pr.cantidad', 'pr.estado', 'pr.pedidos_numero', 'pr.id')
                ->where('pe.NumPedido', '=', $NumPedido)
                ->get();

            $retorno = new stdObject();
            $retorno->cuerpo = $pedido;
            $retorno->detalle = $productos;

            return json_encode($retorno);
        }
        else{
            return redirect('/login/');
        }
    }

}

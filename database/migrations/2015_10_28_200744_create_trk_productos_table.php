<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrkProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trk_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('descipcion');
            $table->string('unidad');
            $table->float('cantidad');
            $table->string('estado');
            $table->string('pedidos_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

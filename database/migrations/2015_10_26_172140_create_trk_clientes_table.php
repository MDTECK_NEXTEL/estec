<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Cliente;

class CreateTrkClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cliente = Cliente =

        Schema::create('trk_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->String('codigo',10)->unique();
            $table->String('usuario')->unique();
            $table->String('nombres');
            $table->String('ruc',11)->unique();
            $table->String('email',50)->unique();
            $table->string('clave', 60);
            $table->text('direccion_fiscal');
            $table->text('direccion_entrega');
            $table->integer('vendedor_id')->unsigned()->nullable();
            $table->foreign('vendedor_id')->references('id')->on('trk_vendedores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrkProductosEspecificaciontecnicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trk_productos_EspecificacionTecnica', function (Blueprint $table) {
            $table->string('url');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id')->on('trk_productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

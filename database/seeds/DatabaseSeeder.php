<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(VendedoresTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(ProductosEspecificaciontecnicaTableSeeder::class);

        Model::reguard();
    }
}

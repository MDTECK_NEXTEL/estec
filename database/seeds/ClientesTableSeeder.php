<?php

use Illuminate\Database\Seeder;
use App\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create(
            [
                'usuario' => 'Jperez',
                'nombres' => 'Juan Perez',
                'codigo' => '2010000556',
                'ruc' => '619887878',
                'email' =>'jperez@ransa.pe',
                'direccion_fiscal' => 'Lima',
                'direccion_entrega' => 'chorrillos'
            ]
        );
        Cliente::create(
            [
                'usuario' => 'TestUser',
                'nombres' => 'usuario muestra',
                'codigo' => '2017007246',
                'ruc' => '619887878',
                'email' =>'estuser@ransa.pe',
                'direccion_fiscal' => 'Lima',
                'direccion_entrega' => 'chorrillos'
            ]
        );
        Cliente::create(
            [
                'usuario' => 'Mendoza',
                'nombres' => 'Joel Mendoza',
                'codigo' => '2048114601',
                'ruc' => '619887878',
                'email' =>'mendoza@ransa.pe',
                'direccion_fiscal' => 'Lima',
                'direccion_entrega' => 'chorrillos'
            ]
        );
        Cliente::create(
            [
                'usuario' => 'Mendoza',
                'nombres' => 'Luis Angel',
                'codigo' => '2049438427',
                'ruc' => '619887878',
                'email' =>'lAngel@ransa.pe',
                'direccion_fiscal' => 'Lima',
                'direccion_entrega' => 'chorrillos'
            ]
        );
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Vendedor;

class VendedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vendedor::create(
            [
                'usuario' => 'Jperez',
                'nombres' => 'Juan Perez',
                'email' =>'jperez@ransa.pe',
                'celular' =>'992318291'
            ]
        );
    }
}

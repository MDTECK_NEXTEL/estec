<?php

use Illuminate\Database\Seeder;
use App\Producto;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create(
            [
                'codigo' => '9898989',
                'descipcion' => 'Flocuantes industriales',
                'unidad' =>'Kgr',
                'cantidad' =>'100',
                'estado' =>'Bueno',
                'pedidos_numero' => '00010000006'
            ]
        );
        Producto::create(
                [
                    'codigo' => '9898989',
                    'descipcion' => 'Flocuantes industriales',
                    'unidad' =>'Kgr',
                    'cantidad' =>'100',
                    'estado' =>'Bueno',
                    'pedidos_numero' => '00010000006'
                ]
        );
        Producto::create(
            [
                'codigo' => '9898989',
                'descipcion' => 'Flocuantes industriales',
                'unidad' =>'Kgr',
                'cantidad' =>'100',
                'estado' =>'Bueno',
                'pedidos_numero' => '00010000009'
            ]
        );
        Producto::create(
        [
            'codigo' => '9898989',
            'descipcion' => 'Flocuantes industriales',
            'unidad' =>'Kgr',
            'cantidad' =>'100',
            'estado' =>'Bueno',
            'pedidos_numero' => '00010000019'
        ]
        );
    }
}

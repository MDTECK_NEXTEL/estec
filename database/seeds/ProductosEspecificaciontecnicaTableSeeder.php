<?php

use Illuminate\Database\Seeder;
use App\ProductoEspecificacionTecnica;

class ProductosEspecificaciontecnicaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductoEspecificacionTecnica::create(
            [
                'url' => 'https://www.google.com/',
                'producto_id' => '1'
            ]
        );

        ProductoEspecificacionTecnica::create(
            [
                'url' => 'https://www.google.com/',
                'producto_id' => '2'
            ]
        );
    }
}

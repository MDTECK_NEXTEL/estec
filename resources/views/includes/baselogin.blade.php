<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>@yield('title','login')</title>

</head>
<body>
@yield('content')
</body>

<link rel="shortcut icon" type="image/x-icon" href="#">
<link href="{!! asset('styles/bootstrap.css') !!}" rel="stylesheet">
<link href="{!! asset('styles/login.css') !!}"  rel="stylesheet">
<script  src="{!! asset('scripts/jquery-1.8.2.min.js') !!}"></script>
<script  src="{!! asset('scripts/bootstrap.js') !!}"></script>
<script  src="{!! asset('scripts/main.js') !!}"></script>
</html>

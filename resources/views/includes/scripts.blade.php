<link href="{!! asset('styles/bootstrap.css') !!}" rel="stylesheet">
<link  href="{!! asset('styles/main.css') !!}" rel="stylesheet">
<link rel="stylesheet" href="{!! asset('styles/font-awesome.min.css') !!}">
<script src="{!! asset('scripts/jquery-1.8.2.min.js') !!}"></script>
<script src="{!! asset('scripts/bootstrap.js') !!}"></script>
<script src="{!! asset('scripts/main.js') !!}"></script>
<script src="{!! asset('scripts/jsSaasa.js') !!}" type="text/javascript"></script>
<script src="{!! asset('scripts/date.js') !!}" type="text/javascript"></script>
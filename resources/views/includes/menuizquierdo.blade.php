<div id="menuprincipal" class="col-md-2">
    <div id="MainMenu">
        <div class="list-group panel">
            <a href="/historial"  id="divimportacion" class="list-group-item"><span class="fa fa-th-list"></span>  Tracking</a>
            <a data-parent="#MainMenu" data-toggle="collapse" class="list-group-item" href="#demo3"><span class="fa fa-key"></span>  Mantenimiento <i class="fa fa-caret-down"></i></a>
            <div id="demo3" class="collapse">
                <a class="list-group-item" href="/cliente" id="divusuario" style="padding-left:40px"><span class="fa fa-group"></span> Clientes</a>
                <a class="list-group-item" href="/vendedor" id="divvendedores" style="padding-left:40px"><span class="fa fa-group"></span> Vendedores</a>
            </div>
            <a href="/contactenos" id="divcontactenos" class="list-group-item"><span class="fa fa-envelope"></span>  Cont&aacute;ctenos</a>
        </div>
    </div>
</div>
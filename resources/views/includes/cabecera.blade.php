<div class="row">
    <div class="col-xs-12">
        <div class="page-header clearfix">
            <div class="col-md-6">
                <div class="pull-left">
                    <a href="home.html">
                        <img class="mg-responsive profile-img-card" src="{!! asset('images/logotipo.jpg') !!}">
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span>Bienvenido : </span>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                            <span class="fa fa-user"></span>
                            {{Auth::user()->name}}
                            <span class="caret"></span>
                        </button>

                        <ul role="menu" class="dropdown-menu">
                            <li><a href="/logout/"><span class="fa fa-sign-in fa-2"></span> Salir </a></li>
                            <li class="divider"></li>
                            <li><a href="#"><span class="fa fa-support"></span> Ayuda</a></li>

                            <li>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            &nbsp;
            <div class="col-md-6">
                <div class="pull-right">
                    <!--
                    {{ $rol or 'rolusuario' }}&nbsp;&nbsp; -&nbsp;&nbsp;
                    <strong>{{ $area or 'areausuario' }}</strong>
                        -->
                </div></div>
        </div>
    </div>
</div>
<!-- Modal Bienvenida-->
<div id="myModalBienvenido" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-envelope"></i>
            </div>
            <div class="modal-title text-uppercase text-left">Bienvenido al <b> sistema de MDTickets para seguimiento de tickets. </b></div>
            <br>
            <div class="modal-body"><p class="text-justify">En este sistema podr&aacute; realizar consultas, reportes, imprimir tus tickets.</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div>
<!--End Info Modal Templates-->
<!--Danger Modal Templates-->

<!-- Modal Mensaje exitosa-->
<div id="myModalExitoso" class="modal modal-message modal-success fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="glyphicon glyphicon-check"></i>
            </div>
            <div class="modal-title text-uppercase text-left">Bienvenido al <b> sistema de SAASA para seguimiento a clientes. </b></div>
            <br>
            <div class="modal-body"><p class="text-justify">En este sistema podr� realizar consultas, reportes, imprimir tus documentos o simular tu proceso.</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div>
<!--End Info Modal Templates-->
<!--Danger Modal Templates-->

<!-- Modal Cambiar Clave-->
<div class="modal fade" id="myModalCambiar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="MypassChangue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <span class="glyphicon glyphicon-lock"></span> Cambiar contrase�a</h4>
            </div>
            <div class="modal-body" >
                <div class="alert alert-warning" style="margin-top:0px;display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">�</a>
                    <strong>Warning!</strong> This alert box indicates a warning that might need attention.
                </div>
                <div style="margin-top:0px" class="alert alert-success h">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    Su contrase�a ha sido <strong> cambiada exitosamente.</strong>
                </div>
                <div class="alert alert-danger" style="margin-top:0px;display:none">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    <strong>Danger!</strong> This alert box indicates a dangerous or potentially negative action.
                </div>
                <div class="alert alert-info" style="margin-top:0px;display:none">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    <strong>Info!</strong> This alert box indicates a neutral informative change or action.
                </div>
                <!-- FORM -->
                <form class="bs-example form-horizontal u-action-sav">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Contrase�a antigua :</label>
                        <div class="col-lg-4">
                            <input type="password" id="inputTextca" class="form-control" placeholder="Contrase�a antigua" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Contrase�a nueva :</label>
                        <div class="col-lg-4">
                            <input type="password" id="inputTextcn" class="form-control" placeholder="contrase�a nueva" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Confirmar contrase�a :</label>
                        <div class="col-lg-4">
                            <input type="password" id="inputTextcc" class="form-control" placeholder="contrase�a nueva" required autofocus>
                        </div>
                    </div>
                    <!-- /FORM -->
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="LoginActualizar();" type="button"><span class="fa fa-save"></span> Guardar</button>
                </form>
                <button data-dismiss="modal" class="btn btn-default" type="button"><span class="fa fa-close"></span>Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="mysessioncaduca" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-warning"></i>
            </div>
            <div class="modal-body"><p class="text-justify">Su session expir� por favor vuelva a ingresar</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" onclick="salida();">OK</button>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div>


<div id="myerror" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-warning"></i>
            </div>
            <div class="modal-body"><p class="text-justify">Ocurrio un error. Por favor informar al area de sistemas.</p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" onclick="EnviarInicio();">OK</button>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div>


<!-- Modal Detalle de volante -->
<div class="modal fade" id="myModalTracking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <span class="fa fa-file-text-o"></span> Detalle de Item - <span class="text-muted">  N&deg; 1 </span></h4>
            </div>
            <div class="panel-body">
                <div class="navbar-collapse  navbar-filter">
                    <div class="col-sm-6">
                        <form class="bs-example form-horizontal">
                            <div class="form-group">
                                <label for="fechai" class="col-lg-6 control-label ff">Orden de compra :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted" id="historial_data_Orden"> </span>
                                </div>
                            </div>
                            <div style="display: block;" class="form-group">
                                <label for="fechaf" class="col-lg-6 control-label ff">Guia de remison :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted" id="historial_data_Guia"> </span>
                                </div>
                            </div>
                            <div style="display: block;" class="form-group">
                                <label for="fechaf" class="col-lg-6 control-label ff">Vendedor :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted" id="historial_data_Vendedor"> </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <form class="bs-example form-horizontal">
                            <div class="form-group">
                                <label for="fechai" class="col-lg-6 control-label ff">Estado :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted label label-success" id="historial_data_Estado"></span>
                                </div>
                            </div>
                            <div style="display: block;" class="form-group">
                                <label for="fechaf" class="col-lg-6 control-label ff">Fecha :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted" id="historial_data_Fecha">  </span>
                                </div>
                            </div>
                            <div style="display: block;" class="form-group">
                                <label for="fechaf" class="col-lg-6 control-label ff">Cliente :</label>
                                <div class="col-lg-6">
                                    <span class="text-muted" id="historial_data_Cliente"> </span>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
                <div class="table-responsive u-table-search">
                    <br>
                    <!-- GRID -->
                    <table class="table table-bordered table-striped table-hover table-responsive " id="historial_data_detalle">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>C&oacute;digo</th>
                            <th>Producto - descripci&oacute;n</th>
                            <th>Unidad medida</th>
                            <th>Cantidad</th>
                            <th>Estado del producto</th>
                            <th>Documento</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <!-- /GRID -->
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="submit"><span class="fa fa-print"></span> Imprimir Pedido</button>
                <button data-dismiss="modal" class="btn btn-default" type="button"><span class="fa fa-close"></span>Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- -->


<!-- Modal Mensaje pregunta de eliminar usuario-->
<div id="myModaleraserusuario" class="modal modal-message modal-warning fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-warning"></i>
            </div>
            <div class="modal-title text-uppercase text-left">� Estas seguro de <b> eliminar Registro ?</b></div>
            <br>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Si</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div>
<!--End Info Modal Templates-->
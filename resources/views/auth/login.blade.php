<!--
<form method="POST" action="/login/">
</div>

<div>
    Password
    <input type="password" name="password" id="password">
</div>

<div>
    <input type="checkbox" name="remember"> Remember Me
</div>

<div>
    <button type="submit">Login</button>
</div>
</form>
-->

@extends('includes.baselogin')
@section('title'){{'Login'}}@endsection
@section('content')
    <form id="formFiltro" method="post"  action="/login/">
        {!! csrf_field() !!}
        <div id="login" class="container">
            <div class="card card-container">
                <div class="fech">
                    <center> Lima,
                        <script>
                            var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                            var f = new Date();
                            document.write(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
                        </script>21 de Octubre de 2015
                    </center>
                </div>
                <img id="profile-img" class="img-responsive profile-img-card" src="{!! asset('images/logotipo.jpg') !!}">

                <input id="ReturnUrl" name="ReturnUrl" type="hidden" value="">
                <!-- <input class="form-control" data-val="true" data-val-required="*" id="Usuario" maxlength="30" name="Usuario" placeholder="Ingrese Usuario" type="text" value=""> -->
                <input class="form-control" data-val="true" data-val-required="*"  id="email" maxlength="30" name="email" placeholder="email" type="text" value="{{ old('email') }}">
                <br>
                <span class="field-validation-valid" data-valmsg-for="Usuario" data-valmsg-replace="true"></span>

                <input class="form-control" data-val="true" data-val-required="*" id="password" maxlength="30" name="password" placeholder="Ingrese Contrase&ntilde;a" type="password">
                <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>


                <br>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Iniciar sesi&oacute;n</button>
                <!-- /form -->
                <a href="#" id="rc" class="forgot-password">
                    <!--   �Has olvidado tu contrase�a? -->
                </a>
            </div>
        </div>
    </form>

    <div id="recuperar" class="container" style="display:none">
        <div class="card card-container">
            <div class="fech"> <center> Lima,
                    <script>
                        var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                        var f = new Date();
                        document.write(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
                    </script>21 de Octubre de 2015 </center>
            </div>
            <img id="profile-img" class="mg-responsive profile-img-card" src="{!! asset('images/logotipo.jpg') !!}">

            <form class="form-signin">

                <input type="text" id="inputText" class="form-control" placeholder="Ingrese ID" required="" autofocus="">
                <br>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Recuperar</button>
            </form><!-- /form -->
            <a id="lg" href="#" class="forgot-password">
                Iniciar sesi&oacute;n
            </a>
        </div>
    </div>
@stop
<!DOCTYPE html>
<!--
<form action="/Clientes/Login/Consultar" autocomplete="off" id="form" method="post" onKeypress="if(event.keyCode == 13) event.returnValue = false;"><input id="login" name="login" type="hidden" value="" /><input id="passwordActual" name="passwordActual" type="hidden" value="" /><input id="PasswordNuevo" name="PasswordNuevo" type="hidden" value="" /><input id="PasswordConfirmacion" name="PasswordConfirmacion" type="hidden" value="" /></form>-->
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Clientes</title>
    <link rel="shortcut icon" type="image/x-icon" href="Content/favicon.ico">

    @include('includes.scripts')

</head>
<body >

<div class="container">
    @include('includes.cabecera', ['rol' => 'Administrador','usuario' => 'usuario Test'])
    <div class="row">
        @include('includes.menuizquierdo')
        <div class="col-md-10" id="contenedorprincipal">

            <form action="/Clientes/Importacion/Consultar" autocomplete="off" id="form" method="post" onKeypress="if(event.keyCode == 13) event.returnValue = false;"><input id="NumeroVuelo" name="NumeroVuelo" type="hidden" value="" />
                <input id="AgenteCarga" name="AgenteCarga" type="hidden" value="" />
                <input id="Consignatario" name="Consignatario" type="hidden" value="" />
                <input id="fechaFin" name="fechaFin" type="hidden" value="" />
                <input data-val="true" data-val-required="Tiene que ingresar una fecha de inicio valida" id="fechaInicio" name="fechaInicio" type="hidden" value="" />
                <input id="NroGuiaAerea" name="NroGuiaAerea" type="hidden" value="" />
                <input id="NroGuiaHija" name="NroGuiaHija" type="hidden" value="" />
                <input id="NroManifiesto" name="NroManifiesto" type="hidden" value="" /><input id="NroVolante" name="NroVolante" type="hidden" value="" /><input data-val="true" data-val-number="El campo TipoUsuario debe ser un n&amp;#250;mero." data-val-required="El campo TipoUsuario es obligatorio." id="TipoUsuario" name="TipoUsuario" type="hidden" value="0" /><input id="Parametro1" name="Parametro1" type="hidden" value="" /><input id="Parametro2" name="Parametro2" type="hidden" value="" /><input id="Parametro3" name="Parametro3" type="hidden" value="" /></form><div class="panel panel-default">

                <div class="panel-heading">
                    <strong><span id="omenu" class="fa fa-chevron-circle-left"></span>  Clientes</strong> <span class="pull-right"><span id="oprincipal" class="fa fa-chevron-circle-up"></span></div>
                <div class="panel-body">

                    <nav role="navigation" class="navbar navbar-default navbar-inverse" id="bprincipal">
                        <!-- Brand and toggle get grouped for better mobile display -->

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="navbar-collapse  navbar-filter">
                            <div class="col-sm-9">
                                <form class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label for="fechai" class="col-lg-1 control-label">Filtrar&nbsp;:</label>
                                        <div class="col-lg-11 pd">
                                            <input type="text" class="form-control" placeholder="ingrese id, Usuario">
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div class="col-sm-3">
                                <div class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label for="nrov" class="col-lg-5 control-label">&nbsp;</label>
                                        <div class="col-lg-6 pd">
                                            <form action="#" metho="post" class="u-form-filter">
                                                <button type="submit" class="larg btn btn-default"><span class="fa fa-search fa-2"></span> Consultar</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.navbar-collapse -->
                    </nav>

                    <div class="panel panel-default text-center h u-filter-loader">
                        <p>Buscando resultados:</p>
                        <div class="progress progress-striped active" style="width: 50%;margin:auto;">
                            <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">45% Complete</span>
                            </div>
                        </div>
                        &nbsp;
                    </div>

                    <div class="table-responsive u-table-search">
                        <!-- GRID -->
                        <div><small>&nbsp;&nbsp;* Para detalle del registro hacer doble Clic.</small></div>
                        <table id="clientes" class="table table-bordered table-striped table-hover table-responsive ">
                            <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" value="">
                                </th>
                                <th>ID Usuario</th>
                                <th>Usuario</th>
                                <th>Nombres</th>
                                <th>RUC</th>
                                <th>Email</th>
                                <th>Direcci&oacute;n Fiscal</th>
                                <th>Direcci&oacute;n Entrega</th>
                                <th>Vendedor</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clientes as $item)
                                <tr>
                                    <td><input type="checkbox" value=""></td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->usuario}}</td>
                                    <td>{{$item->nombres}}</td>
                                    <td>{{$item->ruc}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->direccion_fiscal}}</td>
                                    <td>{{$item->direccion_entrega}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="nav">
                        <div class="pull-left">
                            <form class="pull-left">
                                <a class="btn btn-default" data-toggle="modal" href="#myModaladdusuario"><span class="fa fa-user-plus"></span> Nuevo</a> &nbsp;
                                <a class="btn btn-default" data-toggle="modal" href="#myModaleraserusuario"><span class="fa fa-user-times"></span> Eliminar</a> &nbsp;
                            </form>
                        </div>
                        <div class="pull-right">
                            <ul class="pagination">
                                <li><a href="#">�</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">�</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Detalle de volante -->
            <div class="modal fade" id="myModalvolante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div id="ContenidoDetalle" class="modal-content">

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>


            <div id="mysessioncaduca" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <i class="fa fa-warning"></i>
                        </div>
                        <div class="modal-body"><p class="text-justify">Su session expir� por favor vuelva a ingresar</p></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal" onclick="salida();">OK</button>
                        </div>
                    </div> <!-- / .modal-content -->
                </div> <!-- / .modal-dialog -->
            </div>

        </div>
    </div>

</div>
<!--POPUP ----------------------------------------------------------------------------------------------------------------------------------------------->
@include('includes.modals')


<!-- Modal Agregar Cliente-->
<div class="modal fade" id="myModaladdusuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <span class="fa fa-user-plus"></span> Agregar Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning" style="margin-top:0px;display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">�</a>
                    <strong>Warning!</strong> This alert box indicates a warning that might need attention.
                </div>
                <div style="margin-top:0px;display:none" class="alert alert-success">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    Usuario ha sido <strong> guardado exitosamente.</strong>
                </div>
                <div class="alert alert-danger h" style="margin-top:0px">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    Usuario agregado <strong> ya existe.</strong>
                </div>
                <div class="alert alert-info" style="margin-top:0px;display:none">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    <strong>Info!</strong> This alert box indicates a neutral informative change or action.
                </div>
                <!-- FORM -->
                <form class="bs-example form-horizontal u-action-error">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Usuario :</label>
                        <div class="col-lg-4">
                            <input type="text" id="usufocus" class="form-control" placeholder="jperez" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Clave :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" class="form-control" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Email :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" class="form-control" placeholder="Ransa" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Ruc :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" class="form-control" placeholder="23244323" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Direcc�on fiscal :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" class="form-control" placeholder="Ingrese direcci�n" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Direcci�n de Entrega :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" class="form-control" placeholder="Ingrese direcci�n" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Vendedor :</label>
                        <div class="col-lg-4">
                            <select class="form-control">
                                <option>Carlitos</option>
                                <option>pepito</option>
                                <option>italo</option>
                            </select>
                        </div>
                    </div>
                    <!-- /FORM -->
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span> Guardar</button>
                </form>
                <button data-dismiss="modal" class="btn btn-default" type="button"><span class="fa fa-close"></span>Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal Editar Cliente-->
<div class="modal fade" id="myModalcliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <span class="fa fa-edit"></span> Editar Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning" style="margin-top:0px;display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">�</a>
                    <strong>Warning!</strong> This alert box indicates a warning that might need attention.
                </div>
                <div style="margin-top:0px" class="alert alert-success h">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    Usuario ha sido <strong> guardado exitosamente.</strong>
                </div>
                <div class="alert alert-danger" style="margin-top:0px;display:none">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    <strong>Danger!</strong> This alert box indicates a dangerous or potentially negative action.
                </div>
                <div class="alert alert-info" style="margin-top:0px;display:none">
                    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
                    <strong>Info!</strong> This alert box indicates a neutral informative change or action.
                </div>

                <form id="usu" class="bs-example form-horizontal u-action-sav">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Usuario :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" disabled="disabled" class="form-control" Value="jreyes" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Nombres :</label>
                        <div class="col-lg-4">
                            <input type="text" id="inputText" disabled="disabled" class="form-control" Value="jose reyes" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Clave :</label>
                        <div class="col-lg-4">
                            <input type="password" id="inputText" disabled="disabled" class="form-control" Value="jperez" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Ruc :</label>
                        <div class="col-lg-4">
                            <input type="email" id="inputText" disabled="disabled" class="form-control" Value="12989898" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Email :</label>
                        <div class="col-lg-4">
                            <input type="email" id="inputText" disabled="disabled" class="form-control" Value="jreyres@empresa.com" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Direcci�n Fiscal :</label>
                        <div class="col-lg-4">
                            <input type="email" id="inputText" disabled="disabled" class="form-control" Value="Lima" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Direcci�n Entrega :</label>
                        <div class="col-lg-4">
                            <input type="email" id="inputText" disabled="disabled" class="form-control" Value="Chorrillos" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-5 control-label">
                            Vendedor :</label>
                        <div class="col-lg-4">
                            <select class="form-control" disabled="disabled">
                                <option>Seleccionar</option>
                                <option>Jose Baltazar</option>
                                <option>Luis Montolla</option>
                                <option>Pepito suarez</option>
                                <option>Diegito Ready</option>
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <a id="editusuario" class="btn btn-default"><span class="fa fa-edit"></span> Editar</a>
                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span> Guardar</button>
                </form>
                <button data-dismiss="modal" class="btn btn-default" type="button"><span class="fa fa-close"></span>Cerrar</button>
            </div>
        </div>
    </div>
</div>


</body>

</html>
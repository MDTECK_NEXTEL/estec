<!DOCTYPE html>
<!--
<form action="/Clientes/Login/Consultar" autocomplete="off" id="form" method="post" onKeypress="if(event.keyCode == 13) event.returnValue = false;"><input id="login" name="login" type="hidden" value="" /><input id="passwordActual" name="passwordActual" type="hidden" value="" /><input id="PasswordNuevo" name="PasswordNuevo" type="hidden" value="" /><input id="PasswordConfirmacion" name="PasswordConfirmacion" type="hidden" value="" /></form>-->
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Historial</title>
    <link rel="shortcut icon" type="image/x-icon" href="Content/favicon.ico">

    @include('includes.scripts')

</head>
<body >

<div class="container">
    @include('includes.cabecera', ['rol' => 'Administrador','usuario' => 'usuario Test'])
    <div class="row">
        @include('includes.menuizquierdo')
        <div class="col-md-10" id="contenedorprincipal">

            <form action="/Clientes/Importacion/Consultar" autocomplete="off" id="form" method="post" onKeypress="if(event.keyCode == 13) event.returnValue = false;"><input id="NumeroVuelo" name="NumeroVuelo" type="hidden" value="" /><input id="AgenteCarga" name="AgenteCarga" type="hidden" value="" /><input id="Consignatario" name="Consignatario" type="hidden" value="" /><input id="fechaFin" name="fechaFin" type="hidden" value="" /><input data-val="true" data-val-required="Tiene que ingresar una fecha de inicio valida" id="fechaInicio" name="fechaInicio" type="hidden" value="" /><input id="NroGuiaAerea" name="NroGuiaAerea" type="hidden" value="" /><input id="NroGuiaHija" name="NroGuiaHija" type="hidden" value="" /><input id="NroManifiesto" name="NroManifiesto" type="hidden" value="" /><input id="NroVolante" name="NroVolante" type="hidden" value="" /><input data-val="true" data-val-number="El campo TipoUsuario debe ser un n&amp;#250;mero." data-val-required="El campo TipoUsuario es obligatorio." id="TipoUsuario" name="TipoUsuario" type="hidden" value="0" /><input id="Parametro1" name="Parametro1" type="hidden" value="" /><input id="Parametro2" name="Parametro2" type="hidden" value="" /><input id="Parametro3" name="Parametro3" type="hidden" value="" /></form><div class="panel panel-default">
                <div class="panel-heading">
                    <strong><span id="omenu" class="fa fa-chevron-circle-left"></span>  Historial de Pedidos</strong> <span class="pull-right"><span id="oprincipal" class="fa fa-chevron-circle-up"></span></div>
                <div class="panel-body">

                    <nav id="bprincipal" class="navbar navbar-default navbar-inverse" role="navigation">
                        <div class="navbar-collapse  navbar-filter">
                            <div class="col-sm-3">
                                <form class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-6 control-label" for="fechai">Fecha de inicio:</label>
                                        <div class="col-lg-6 pd">
                                            <input type="date" placeholder="dd/mm/aaaa" id="fechai" value="01/01/2015" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-3">
                                <form class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-6 control-label" for="fechai">Fecha fin:</label>
                                        <div class="col-lg-6 pd">
                                            <input type="date" placeholder="dd/mm/aaaa" id="fechai" value="30/01/2015" title="" data-toggle="tooltip" class="form-control" data-original-title="(Rango max. 30 d�as)">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-3">
                                <form class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-6 control-label" for="nrov">Filtrar :</label>
                                        <div class="col-lg-6 pd">
                                            <input type="text" placeholder="0000000000" id="inputEmail1" value="917" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-3">
                                <div class="bs-example form-horizontal">
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label" for="nrov">&nbsp;</label>
                                        <div class="col-lg-6 pd">
                                            <form class="u-form-filter" metho="post" action="#">
                                                <button class="larg btn btn-default" type="submit"><span class="fa fa-search fa-2"></span> Consultar</button>
                                            </form>
                                            <br>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </nav>

                    <div class="panel panel-default text-center h u-filter-loader">
                        <p>Buscando resultados:</p>
                        <div class="progress progress-striped active" style="width: 50%;margin:auto;">
                            <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">45% Complete</span>
                            </div>
                        </div>
                        &nbsp;
                    </div>

                    <div class="table-responsive u-table-search">
                        <!-- GRID -->
                        <div><small>&nbsp;&nbsp;* Para detalle del registro hacer doble Clic.</small></div>
                        <table id="historial" class="table table-bordered table-striped table-hover table-responsive ">
                            <thead>
                            <tr>
                                <th>
                                    &Iacute;tem
                                </th>
                                <th>Orden de Compra</th>
                                <th>Gu&iacute;a de Remisi&oacute;n</th>
                                <th>Estado</th>
                                <th>Fecha</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historial as $item)
                                <tr>
                                    <td>{{$item->NumPedido}}</td>
                                    <td>{{$item->OrdenCompra}}</td>
                                    <td>{{$item->NumGuiaRemision}}</td>
                                    @if($item->Estado == "APROBADO")
                                        <td><span class="label label-warning">Aprobado</span></td>
                                    @elseif($item->Estado =="DESPACHADO")
                                        <td><span class="label label-success">Despachado</span></td>
                                    @elseif($item->Estado =="GENERADO")
                                        <td><span class="label label-danger">Generado</span></td>
                                    @else
                                        <td><span class="label"></span></td>
                                    @endif
                                        <td>{{$item->FechaPedido}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /GRID -->
                    </div>
                    <div class="nav">

                        <div class="pull-right">
                            <ul class="pagination">
                                <li><a href="#">&laquo;</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Modal Detalle de volante -->
            <div class="modal fade" id="myModalvolante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div id="ContenidoDetalle" class="modal-content">

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>


            <div id="mysessioncaduca" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <i class="fa fa-warning"></i>
                        </div>
                        <div class="modal-body"><p class="text-justify">Su session expir� por favor vuelva a ingresar</p></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal" onclick="salida();">OK</button>
                        </div>
                    </div> <!-- / .modal-content -->
                </div> <!-- / .modal-dialog -->
            </div>

        </div>
    </div>

</div>
<!--POPUP ----------------------------------------------------------------------------------------------------------------------------------------------->
@include('includes.modals')

</body>
<script>
    $(window).load(function(){
        $(document).ready(function(){
            $('#myModalBienvenido').modal('show')
        });
    });//]]>
</script>

</html>